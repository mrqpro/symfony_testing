<?php
/**
 * Created by PhpStorm.
 * User: quang
 * Date: 8/27/18
 * Time: 9:41 AM
 */

namespace AppBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Symfony\Component\HttpFoundation\Response;

class ExceptionController extends \Symfony\Bundle\TwigBundle\Controller\ExceptionController {

    public function __construct(Environment $twig, $debug)
    {
        parent::__construct($twig, $debug);
    }

    /**
     * @param Request $request
     * @param FlattenException $exception
     * @param DebugLoggerInterface|null $logger
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function showExceptionAction(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null) {
        $currentContent = $this->getAndCleanOutputBuffering($request->headers->get('X-Php-Ob-Level', -1));
        $showException = $request->attributes->get('showException', $this->debug); // As opposed to an additional parameter, this maintains BC
        $code = $exception->getStatusCode();

        if ($exception->getClass() == UniqueConstraintViolationException::class) {
            $template = '@Twig/Exception/' . 'duplicated_data.html.twig';
        } elseif ($exception->getClass() == AccessDeniedHttpException::class) {
            $template = '@Twig/Exception/' . 'access_denied.html.twig';
        }
        else {
            $template = (string) $this->findTemplate($request, $request->getRequestFormat(), $code, $showException);
        }

        return new Response($this->twig->render(
            $template,
            array(
                'status_code' => $code,
                'status_text' => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '',
                'exception' => $exception,
                'logger' => $logger,
                'currentContent' => $currentContent,
            )
        ), 200, array('Content-Type' => $request->getMimeType($request->getRequestFormat()) ?: 'text/html'));
    }

}