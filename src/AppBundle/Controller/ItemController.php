<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Item;
use Cocur\Slugify\Slugify;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Item controller.
 *
 * @Route("item")
 */
class ItemController extends Controller
{
    public $slug;

    public function __construct()
    {
        $this->slug = new Slugify();
    }

    /**
     * Lists all item entities.
     *
     * @Route("/list", name="item_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        // Check permission
        if (in_array('ROLE_ADMIN', $user->getRoles()) || in_array('ROLE_SUPER_ADMIN', $user->getRoles())) {
            $items = $em->getRepository('AppBundle:Item')->findAll();
        } else {
            $items = $em->getRepository('AppBundle:Item')->findItemByUser($user);
        }
        return $this->render('@App/item/index.html.twig', array(
            'items' => $items,
        ));
    }

    /**
     * Creates a new item entity.
     *
     * @Route("/new", name="item_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $item = new Item();
        $form = $this->createForm('AppBundle\Form\ItemType', $item);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $item->setSlug($this->slug->slugify($item->getName()));
            $em = $this->getDoctrine()->getManager();
            $error = false;
            try {
                $user = $this->getUser();
                $item->setUser($user);
                $em->persist($item);
                $em->flush();
            } catch (UniqueConstraintViolationException $ex) {
                $error = true;
                $this->addFlash('success', 'Duplicated data : '.$ex->getMessage());
            } catch (\Exception $ex) {
                $error = true;
                $this->addFlash('success', 'Error : '.$ex->getMessage());
            }

            if (false == $error) {
                $this->addFlash('success', 'New item has been created successfully');
            }

            return $this->redirectToRoute('item_index', array('id' => $item->getId()));
        }

        return $this->render('@App/item/new.html.twig', array(
            'item' => $item,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a item entity.
     *
     * @Route("/{id}", name="item_show")
     * @Method("GET")
     */
    public function showAction(Item $item)
    {
        return $this->render('@App/item/show.html.twig', array(
            'item' => $item,
        ));
    }

    /**
     * Displays a form to edit an existing item entity.
     *
     * @Route("/{id}/edit", name="item_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Item $item)
    {
        $user = $this->getUser();
        if (NULL == $item->getUser() || $user->getId() != $item->getUser()->getId()) {
            $this->addFlash('success', 'You do not have permission to perform this action');
            return $this->redirectToRoute('item_index', array('id' => $item->getId()));
        }
        $editForm = $this->createForm('AppBundle\Form\ItemType', $item);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $item->setSlug($this->slug->slugify($item->getName()));
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Item "'.$item->getName().'" has been updated successfully');

            return $this->redirectToRoute('item_index', array('id' => $item->getId()));
        }

        return $this->render('@App/item/edit.html.twig', array(
            'item' => $item,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a item entity.
     *
     * @Route("/delete/{id}", name="item_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Item $item)
    {
        $user = $this->getUser();
        if (NULL == $item->getUser() || $user->getId() != $item->getUser()->getId()) {
            $this->addFlash('success', 'You do not have permission to perform this action');
            return $this->redirectToRoute('item_index', array('id' => $item->getId()));
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();

        $this->addFlash('success', 'Category "'.$item->getName().'" has been removed successfully');

        return $this->redirectToRoute('item_index');
    }
}
