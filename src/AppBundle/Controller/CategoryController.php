<?php

namespace AppBundle\Controller;

use Cocur\Slugify\Slugify;
use AppBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

/**
 * Category controller.
 *
 * @Route("category")
 */
class CategoryController extends Controller
{
    public $slug;

    public function __construct()
    {
        $this->slug = new Slugify();
    }

    /**
     * Lists all category entities.
     *
     * @Route("/list", name="category_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('AppBundle:Category')->findAll();

        return $this->render('@App/Category/index.html.twig', array(
            'categories' => $categories,
        ));
    }

    /**
     * Creates a new category entity.
     *
     * @Route("/add", name="category_add")
     */
    public function addAction(Request $request)
    {
        $category = new Category();
        $category->setSlug('temp_value');
        $form = $this->createForm('AppBundle\Form\CategoryType', $category);
        $form->add('submit', SubmitType::class, [
            'label' => 'Submit',
            'attr' => ['class' => 'btn btn-primary'],
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $category->setSlug($this->slug->slugify($category->getName()));
            $em = $this->getDoctrine()->getManager();
            $error = false;
            try {
                $em->persist($category);
                $em->flush();
            } catch (UniqueConstraintViolationException $ex) {
                $error = true;
                $this->addFlash('success', 'Duplicated data : '.$ex->getMessage());
            } catch (\Exception $ex) {
                $error = true;
                $this->addFlash('success', 'Error : '.$ex->getMessage());
            }

            if (false == $error) {
                $this->addFlash('success', 'New category has been created successfully');
            }

            return $this->redirectToRoute('category_index');
        }

        return $this->render('@App/Category/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a category entity.
     *
     * @Route("/{id}", name="category_show")
     */
    public function showAction(Category $category)
    {
        return $this->render('@App/Category/show.html.twig', array(
            'category' => $category,
        ));
    }

    /**
     * Displays a form to edit an existing category entity.
     *
     * @Route("/edit/{id}", name="category_edit")
     */
    public function editAction(Request $request, Category $category)
    {
        // Preparing form data
        $editForm = $this->createForm('AppBundle\Form\CategoryType', $category);
        $editForm->add('submit', SubmitType::class, [
            'label' => 'Submit',
            'attr' => ['class' => 'btn btn-primary'],
        ]);
        $editForm->remove('slug');

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $category->setSlug($this->slug->slugify($category->getName()));
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Category "'.$category->getName().'" has been updated successfully');

            return $this->redirectToRoute('category_index');
        }

        return $this->render('@App/Category/edit.html.twig', array(
            'category' => $category,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a category entity.
     *
     * @Route("/delete/{id}", name="category_delete")
     */
    public function deleteAction(Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();
        // Adding flash message
        $this->addFlash('success', 'Category "'.$category->getName().'" has been removed successfully');

        return $this->redirectToRoute('category_index');
    }
}
