<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AppBundle:User')->findAllNormalUser();

        return $this->render('user/index.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->createForm('AppBundle\Form\UserTypeAddNew', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                return $this->redirectToRoute('user_index', array('id' => $user->getId()));
            } catch (UniqueConstraintViolationException $ex) {
                $this->addFlash('error', 'Duplicated data');
            } catch (\Exception $ex) {
                //die($ex->getMessage());
                $this->addFlash('errror', 'Error for inserting new user');
            }
        }

        return $this->render('user/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}", name="user_show", requirements={"id"="\d+"})
     * @Method("GET")
     */
    public function showAction(User $user)
    {
        return $this->render('user/show.html.twig', array(
            'user' => $user,
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {
        $editForm = $this->createForm('AppBundle\Form\UserType', $user);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if ('' != $request->get('password')) {
                $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
                $new_pwd_encoded = $encoder->encodePassword($request->get('password'), $user->getSalt());
                $user->setPassword($new_pwd_encoded);
            } else {
                $user->__unset('password');
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index', array('id' => $user->getId()));
        }

        return $this->render('user/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/delete/{id}", name="user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('user_index');
    }

    /**
     * @Annotation
     * @Route("/profile", name="user_profile")
     * @Method({"GET", "POST"})
     */
    public function updateAction(Request $request)
    {
        $user = $this->getUser();
        $editForm = $this->createForm('AppBundle\Form\UserType', $user);
        $editForm->remove('roles');
        $editForm->handleRequest($request);

         if ($editForm->isSubmitted() && $editForm->isValid()) {
             if ('' != $request->get('password')) {
                 $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
                 $new_pwd_encoded = $encoder->encodePassword($request->get('password'), $user->getSalt());
                 $user->setPassword($new_pwd_encoded);
             } else {
                 $user->__unset('password');
             }
             $this->getDoctrine()->getManager()->flush();
             $this->addFlash('success', 'Your profile has been updated successfully');
             return $this->redirectToRoute('user_profile', array('id' => $user->getId()));
         }

        return $this->render('user/update_profile.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
        ));

    }
}
